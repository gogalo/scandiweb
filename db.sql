-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 18, 2021 at 01:16 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `products`
--

-- --------------------------------------------------------

--
-- Table structure for table `product_list`
--

CREATE TABLE `product_list` (
  `id` int(11) NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sku` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_type` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_list`
--

INSERT INTO `product_list` (`id`, `product_name`, `product_price`, `sku`, `product_type`, `details`) VALUES
(40, 'hurry potter', '20$', 'dnsakj34', 'Book', 'a:5:{s:4:\"size\";s:1:\" \";s:6:\"weight\";s:3:\"1,4\";s:6:\"height\";s:1:\" \";s:5:\"width\";s:1:\" \";s:6:\"length\";s:1:\" \";}'),
(41, 'Blade Runner', '20$', 'sadhk54', 'DVD-Disc', 'a:5:{s:4:\"size\";s:6:\"400 mb\";s:6:\"weight\";s:1:\" \";s:6:\"height\";s:1:\" \";s:5:\"width\";s:1:\" \";s:6:\"length\";s:1:\" \";}'),
(42, 'chair', '50$', 'ssn343', 'Furniture', 'a:5:{s:4:\"size\";s:1:\" \";s:6:\"weight\";s:1:\" \";s:6:\"height\";s:2:\"60\";s:5:\"width\";s:2:\"20\";s:6:\"length\";s:2:\"10\";}'),
(43, '', '', '', 'DVD-Disc', 'a:5:{s:4:\"size\";s:1:\" \";s:6:\"weight\";s:1:\" \";s:6:\"height\";s:1:\" \";s:5:\"width\";s:1:\" \";s:6:\"length\";s:1:\" \";}'),
(48, 'The avengers', '50$', 'dsakjh', 'DVD-Disc', 'a:5:{s:4:\"size\";s:4:\"2000\";s:6:\"weight\";s:1:\" \";s:6:\"height\";s:1:\" \";s:5:\"width\";s:1:\" \";s:6:\"length\";s:1:\" \";}');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `product_list`
--
ALTER TABLE `product_list`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `product_list`
--
ALTER TABLE `product_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
