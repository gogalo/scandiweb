<?php require_once('header.php');?>
<div class="wrapper">
	<div class="product_add_container">
		<form id="product_form" method="POST" name="productAdd">
			<h2 class="edit_title">Edit Product</h2>
			<button class="submit_button" id="submit">Save</button>
			<hr>

			<div class="req_field_note_hide" id="rec_fields">
				<span>Please fill all required fields!</span>
			</div>


			<div class="success">
				<span>New product added</span>
			</div>
			<div class="insert_error">
				<span>Error while adding new product</span>
			</div>

			<div class="input_container">
				<input type="text" name="SKU" placeholder="SKU" class="swinput" id="SKU">
				<input type="text" name="name" placeholder="Product Name" class="swinput" id="name">

				<span class="notes">Please use currancy sign exampleL 20$</span>
				<input type="text" name="price" placeholder="Price" class="swinput" id="price">

				<select name="type" class="swselect" id="type">
					<option value="">Type</option>
					<option value="DVD-Disc">DVD-Disc</option>
					<option value="Book">Book</option>
					<option value="Furniture">Furniture</option>
				</select>
			</div>
			<div class="input_container_type_res">
				<div id="detail_result"></div>
			</div>
		</form>

		
	</div>
</div>
<?php require_once('footer.php');?>