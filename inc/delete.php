<?php
	require_once('db_connection.php');
	require_once('functions.php');

	$deleted = '';
	$result = '';
	$checked_values = $_POST['check_'];


	foreach ($checked_values as $checked_value) :
			$delete = $db->query('DELETE FROM product_list WHERE id = ?', $checked_value);
			$deleted = $delete->query_count;
	endforeach;
		
	if ($deleted) :
		$result = 'Deleted';
	else:
		$result = 'Unable to delete';
	endif;

	echo $result;
?>