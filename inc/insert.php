<?php
	require_once('db_connection.php');
	require_once('functions.php');


	$result = '';
	$detailsIn = array();
	$SKUIn = validate_input($_POST['SKU']); 
	$nameIn = validate_input($_POST['name']); 
	$priceIn = validate_input($_POST['price']); 
	$typeIn = validate_input($_POST['type']); 

	if (isset($_POST['size']) && $_POST['size'] != '') {
		$sizeIn = validate_input($_POST['size']);
	}else{
		$sizeIn = ' ';
	}

	if (isset($_POST['weight']) && $_POST['weight'] != '') {
		$weightIn = validate_input($_POST['weight']);
	}else{
		$weightIn = ' ';
	}

	if (isset($_POST['height']) && $_POST['height'] != '') {
		$heightIn = validate_input($_POST['height']);
	}else{
		$heightIn = ' ';
	}

	if (isset($_POST['width']) && $_POST['width'] != '') {
		$widthIn = validate_input($_POST['width']);
	}else{
		$widthIn = ' ';
	}

	if (isset($_POST['length']) && $_POST['length'] != '') {
		$lengthIn = validate_input($_POST['length']);
	}else{
		$lengthIn = ' ';
	}

	$detailsIn = array(
		'size' => $sizeIn, 
		'weight' => $weightIn, 
		'height' => $heightIn, 
		'width' => $widthIn, 
		'length' => $lengthIn
	);


	$insert = $db->query('INSERT INTO product_list (product_name,product_price,sku,product_type,details) VALUES (?,?,?,?,?)', $nameIn, $priceIn, $SKUIn, $typeIn, serialize($detailsIn));


	if ($insert->affectedRows() == 1) :
		$result = 'Saved';
	else:
		$result = 'Unable to save';
	endif;

	echo $result;
?>