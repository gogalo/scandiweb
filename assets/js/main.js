jQuery( document ).ready(function() {

	//getting product type detail input
	jQuery("#type").change(function() {
		jQuery.ajax({
            type: "POST",
            url: "inc/get-type.php",
            data: {
                ProductType: this.value,
            },
            success: function (response) {
                jQuery('#detail_result').html(response);
                jQuery('#detail_result').fadeIn('fast');
        	}
    	});//ajax
	});//change



jQuery("#submit").click(function(){

	$("form[name='productAdd']").validate({
	    rules: {

	      SKU: {
	        required: true,
	        maxlength: 8,
	        minlength: 5
	      },

	      name: {
	        required: true,
	        maxlength: 30
	      },

	      price: {
	        required: true,
	      },

	      type: {
	        required: true,
	      },

	      size: {
	        required: true,
	      },

	      weight: {
	        required: true,
	      },

	      height: {
	        required: true,
	        number: true
	      },

	      width: {
	        required: true,
	        number: true
	      },

	      length: {
	        required: true,
	        number: true
	      }
	     
	    },
	    // Specify validation error messages
	    messages: {
	      weight: "Please enter weight",
	      size: "Please enter size",
	      type: "Please select type",
	      price: "Please enter price",
	      SKU: {
	        required: "Please provide SKU",
	        minlength: "SKU must be at least 5 length",
	        maxlength: "SKU can not be greater than 8"
	      },
	      name: {
	        required: "Please provide product name",
	        maxlength: "name can not be greater than 30"
	      },
	      height: {
	        required: "Please provide product height",
	        number: "Please enter correct value"
	      },

	      width: {
	        required: "Please provide product width",
	        number: "Please enter correct value"
	      },

	      length: {
	        required: "Please provide product width",
	        number: "Please enter correct value"
	      }
	    },
	    // Make sure the form is submitted to the destination defined
	    // in the "action" attribute of the form when valid
	    submitHandler: function(form) {
	      
			var productData = jQuery("#product_form").serialize();

		    jQuery.ajax({
		        type: "POST",
		        url: "inc/insert.php",
		        data: productData,
		        success: function(response) {
		            if(jQuery.trim(response) === 'Saved'){
		        		jQuery(".success").css("display", "block");
		        	}else{
		        		jQuery(".insert_error").css("display", "block");
		        	}
		        }
		    });
	    }//submitHandler

	});

});	//click


	//bulk check
	jQuery("#checkAl").click(function () {
		jQuery('input:checkbox').not(this).prop('checked', this.checked);
	});


	//deleting products
	jQuery("#delete").click(function(){
	  	
		var check = [];
        jQuery.each(jQuery(".checkbox:checked"), function(){
            check.push(jQuery(this).val());
        });
		var action = 'delete';

		jQuery.ajax({
            type: "POST",
            url: "inc/delete.php",
            data: {
                check_: check,
                action_: action
            },
            success: function (response) {
            	if(jQuery.trim(response) === 'Deleted'){
            		jQuery(".success").css("display", "block");
            	}else{
            		jQuery(".insert_error").css("display", "block");
            	}
        	}
    	});//ajax

	});


});//document

