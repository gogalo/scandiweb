<?php
require_once('inc/db_connection.php');
require_once('inc/functions.php');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Scandiweb</title>
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<script src="assets/js/jquery.js"></script>
</head>
<body>
<ul>
  <li><a class="active" href="index.php">Product List</a></li>
  <li><a href="edit-product.php">Edit Product</a></li>
</ul>
