<?php require_once('header.php');?>
<div class="wrapper">
	<div class="product_add_container">
		<h2 class="edit_title">Products</h2>
		<button class="delete_button" id="delete">Delete</button>
		<hr>


		<div class="success">
			<span>Item deleted</span>
		</div>
		<div class="insert_error">
			<span>Error while deleting item</span>
		</div>

		<div class="sel_all">
			<input type="checkbox" id="checkAl"><span>Select all</span>
		</div>

		<div class="product_perent">
			
			<?php 

				$products = $db->query('SELECT * FROM product_list')->fetchAll();
		
				foreach ($products as $product) :?>


						<div class="product_child">
							<div class="product_check"><input type="checkbox" name="check[]" value="<?= $product["id"]; ?>" class="checkbox"></div>
							<p><?= $product['sku']; ?></p>
							<p><?= $product['product_name']; ?></p>
							<p><?= $product['product_price']; ?></p>
							<p>

							<?php 

								echo get_type($product['product_type']);
									
								$details = unserialize($product['details']);
								$type = $product['product_type'];

								switch ($type) :
										case 'DVD-Disc':
											echo $details['size'];
											break;

										case 'Book':
											echo $details['weight'];
											break;

										case 'Furniture':
											echo $details['height'].'x'.$details['width'].'x'.$details['length'];
											break;

								endswitch;	

							?>

							</p>
						</div>




				<?php endforeach;?>

		</div>
		
	</div>
</div>
<?php require_once('footer.php');?>